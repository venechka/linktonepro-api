<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->string('name');
            $table->string('logo_picture')->nullable()->default(null);;
            $table->string('picture')->nullable()->default(null);
            $table->unsignedBigInteger('artist_types_id');
            $table->unsignedBigInteger('genres_id');
            $table->string('description')->nullable()->default(null);
            $table->string('country');
            $table->string('city');
            $table->json('members')->nullable()->default(null);
            $table->json('social_links')->nullable()->default(null);
            $table->json('banners')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
};
