<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concerts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artist_id');
            $table->dateTime('start_datetime');
            $table->string('country');
            $table->string('city');
            $table->string('venue');
            $table->integer('concert_type_id');
            $table->json('ticket_links')->nullable()->default(null);
            $table->unsignedBigInteger("poster_file_id")->nullable()->default(null);
            $table->string('video_link')->nullable()->default(null);;
            $table->string('description')->nullable()->default(null);
            $table->json('tags')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concerts');
    }
};
