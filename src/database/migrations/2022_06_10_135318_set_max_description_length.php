<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string('description', 500)->change();
        });

        Schema::table('releases', function (Blueprint $table) {
            $table->string('description', 500)->change();
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->string('description', 500)->change();
        });

        Schema::table('concerts', function (Blueprint $table) {
            $table->string('description', 500)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string('description', 255)->change();
        });

        Schema::table('releases', function (Blueprint $table) {
            $table->string('description', 255)->change();
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->string('description', 255)->change();
        });

        Schema::table('concerts', function (Blueprint $table) {
            $table->string('description', 255)->change();
        });
    }
};
