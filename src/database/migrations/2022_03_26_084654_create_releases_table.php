<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releases', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('banned')->default(false);
            $table->unsignedBigInteger('artist_id');
            $table->unsignedBigInteger('cover_file_id');
            $table->integer('release_type_id');
            $table->string('description')->nullable()->default(null);
            $table->date('publish_date')->nullable()->default(null);
            $table->string('label_company');
            $table->json('tracks')->nullable()->default(null);
            $table->json('links')->nullable()->default(null);
            $table->json('tags')->nullable()->default(null);
            $table->unsignedBigInteger('views_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('releases');
    }
};
