<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->dropColumn('picture');
            $table->dropColumn('logo_picture');
            $table->dropColumn('banners');
            $table->unsignedBigInteger('picture_file_id')->nullable()->default(null);
            $table->unsignedBigInteger('logo_file_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string('logo_picture')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->json('banners')->nullable()->default(null);
            $table->dropColumn('picture_file_id');
            $table->dropColumn('logo_file_id');
        });
    }
};
