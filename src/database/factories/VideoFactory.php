<?php

namespace Database\Factories;

use App\Models\Release;
use App\Models\VideoType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Video>
 */
class VideoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $videoType = random_int(1, VideoType::count() - 1);

        $tagsCount = random_int(1, 5);
        $tags = [];
        for ($i = 0; $i < $tagsCount; $i++)
            $tags[] = $this->faker->word();

//        $release_id = random_int(0, Release::count() - 1);

        return [
            "name" => $this->faker->company(),
            "link" => "https://youtu.be/Iiqf2R462lo",
            "video_type_id" => $videoType,
            "release_id" => null, //$release_id == 0 ? null : $release_id,
            "publish_date" => Carbon::yesterday(),
            "description" => $this->faker->realTextBetween(100, 255),
            "tags" => json_encode($tags)
        ];
    }
}
