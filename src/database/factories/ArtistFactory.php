<?php

namespace Database\Factories;

use App\Models\ArtistType;
use App\Models\Genre;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Artist>
 */
class ArtistFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $artistName = $this->faker->company();
        $artistType = random_int(1, ArtistType::count() - 1);
        $genre = random_int(1, Genre::count() - 1);

        $members = [
            [
                'job' => $this->faker->jobTitle(),
                'name' => $this->faker->firstName().' '.$this->faker->lastName(),
                'link' => $this->faker->url(),
                'social' => 'vk'
            ],
            [
                'job' => $this->faker->jobTitle(),
                'name' => $this->faker->firstName().' '.$this->faker->lastName(),
                'link' => $this->faker->url(),
                'social' => 'vk'
            ],
            [
                'job' => $this->faker->jobTitle(),
                'name' => $this->faker->firstName().' '.$this->faker->lastName(),
                'link' => $this->faker->url(),
                'social' => 'vk'
            ],
            [
                'job' => $this->faker->jobTitle(),
                'name' => $this->faker->firstName().' '.$this->faker->lastName(),
                'link' => $this->faker->url(),
                'social' => 'vk'
            ],
        ];

        $socialLinks =  [
            "ok" => $this->faker->url(),
            "vk" => $this->faker->url(),
            "web" => $this->faker->url(),
            "youtube" => $this->faker->url(),
            "telegram" => $this->faker->url()
        ];

        return [
            'name' => $artistName,
            'artist_types_id' => $artistType,
            'genres_id' => $genre,
            'description' => $this->faker->realTextBetween(100, 255),
            'country' => $this->faker->country(),
            'city' => $this->faker->city(),
            'members' => json_encode($members),
            'social_links' => json_encode($socialLinks),
            'is_banned' => random_int(0,1),
            'picture_file_id' => (new FileService())->getRandomPicture('artist_pictures', 'Artist', 1000,200, 'background'),
            'logo_file_id' => (new FileService())->getRandomPicture('artist_pictures', 'Artist', 300,300, 'logo'),
            'slug' => Str::slug($artistName)
        ];
    }
}
