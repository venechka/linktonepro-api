<?php

namespace Database\Factories;

use App\Models\ConcertType;
use App\Services\FileService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Concert>
 */
class ConcertFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $concertType = random_int(1, ConcertType::count() - 1);

        $tagsCount = random_int(1, 5);
        $tags = [];
        for ($i = 0; $i < $tagsCount; $i++)
            $tags[] = $this->faker->word();

        return [
            "start_datetime" => new Carbon($this->faker->dateTimeThisYear()),
            "country" => $this->faker->country(),
            "city" => $this->faker->city(),
            "venue" => $this->faker->company(),
            "concert_type_id" => $concertType,
            "ticket_links" => null,
            "poster_file_id" => (new FileService())->getRandomPicture('concert_posters', 'Concert', 200,200, 'poster'),
            "video_link" => "https://youtu.be/Iiqf2R462lo",
            "description" => $this->faker->realTextBetween(100, 255),
            "tags" => json_encode($tags)
        ];
    }
}
