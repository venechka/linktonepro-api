<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Story>
 */
class StoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $filepath = 'sample_960x540.mp4';
        $file = new File([
            "model" => "Story",
            "original_name" => "story",
            "path" => $filepath
        ]);
        $file->save();

        return [
            "uploaded_by_user_id" => 1,
            "file_id" => $file->id
        ];
    }
}
