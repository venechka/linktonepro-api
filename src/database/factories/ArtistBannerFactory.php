<?php

namespace Database\Factories;

use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ArtistBanner>
 */
class ArtistBannerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'uploaded_by_user_id' => 1,
            'file_id' => (new FileService())->getRandomPicture('banners_pictures', 'ArtistBanner', 200,200, 'banner'),
            'name' => $this->faker->word(),
            'link' => $this->faker->url()
        ];
    }
}
