<?php

namespace Database\Factories;

use App\Models\ReleaseType;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Release>
 */
class ReleaseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $releaseType = random_int(1, ReleaseType::count() - 1);

        $socialLinks =  [
            "yandex" => $this->faker->url(),
            "vk" => $this->faker->url(),
            "ok" => $this->faker->url(),
            "sber" => $this->faker->url(),
            "tiktok" => $this->faker->url(),
            "apple" => $this->faker->url(),
            "spotify" => $this->faker->url(),
            "youtube" => $this->faker->url(),
            "deezer" => $this->faker->url()
        ];

        $trackCount = random_int(1, 10);
        $tracks = [];
        for ($i = 0; $i < $trackCount; $i++)
            $tracks[] = $this->faker->company();

        $tagsCount = random_int(1, 5);
        $tags = [];
        for ($i = 0; $i < $tagsCount; $i++)
            $tags[] = $this->faker->word();

        return [
            "name" => $this->faker->company(),
            "is_banned" => random_int(0,1),
            "cover_file_id" => (new FileService())->getRandomPicture('release_covers', 'Release', 200,200, 'cover'),
            "release_type_id" => $releaseType,
            'description' => $this->faker->realTextBetween(100, 255),
            "publish_date" => Carbon::yesterday(),
            "label_company" => $this->faker->company(),
            "tracks" => json_encode($tracks),
            "social_links" => json_encode($socialLinks),
            "tags" => json_encode($tags)
        ];
    }
}
