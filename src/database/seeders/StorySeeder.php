<?php

namespace Database\Seeders;

use App\Models\Concert;
use App\Models\Release;
use App\Models\Story;
use App\Models\Video;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $releases = Release::all();
        foreach ($releases as $release)
            Story::factory()
                ->count(random_int(0,3))
                ->for($release)
                ->create();

        $videos = Video::all();
        foreach ($videos as $video)
            Story::factory()
                ->count(random_int(0,3))
                ->for($video)
                ->create();

        $concerts = Concert::all();
        foreach ($concerts as $concert)
            Story::factory()
                ->count(random_int(0,3))
                ->for($concert)
                ->create();

    }
}
