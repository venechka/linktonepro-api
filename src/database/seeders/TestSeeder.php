<?php

namespace Database\Seeders;

use App\Models\Artist;
use App\Models\ArtistBanner;
use App\Models\Concert;
use App\Models\Release;
use App\Models\Video;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "admin",
            "email" => "g-worlds@ya.ru",
            'email_verified_at' => now(),
            "password" => Hash::make("123123123"),
            "is_admin" => true
        ]);

        User::create([
            "name" => "public-api",
            "email" => "support@linktonepro.site",
            'email_verified_at' => now(),
            "password" => Hash::make("123123123"),
            "is_admin" => false
        ]);

        $user = User::find(2);
        $token = $user->createToken('api');

//        echo 'Public API Token '.$token->plainTextToken;
//        echo PHP_EOL;

        $this->command->info('Public API Token '.$token->plainTextToken);

        $this->call(DictionariesSeeder::class);
        $this->call(GeoSeeder::class);

        User::create([
            "name" => "Arrested",
            "lastname" => "Development",
            "email" => "arrested.development@ya.ru",
            'email_verified_at' => now(),
            "password" => Hash::make("123123123"),
            "phone" => '79199199191',
            "country" => 'Россия',
            "city" => 'Ростов-на-Дону'
        ]);

        User::factory()
            ->count(10)
            ->hasArtists(2)
            ->create();

        $artists = Artist::all();
        foreach ($artists as $artist) {
            Release::factory()
                ->count(random_int(0, 3))
                ->for($artist)
                ->create();
            ArtistBanner::factory()
                ->count(random_int(0, 3))
                ->for($artist)
                ->create();
            Video::factory()
                ->count(random_int(0, 3))
                ->for($artist)
                ->create();
            Concert::factory()
                ->count(random_int(0, 3))
                ->for($artist)
                ->create();
        }

        $this->call(StorySeeder::class);
        $this->call(SlugSeeder::class);
    }
}
