<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
   /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "admin",
            "email" => "g-worlds@ya.ru",
            'email_verified_at' => now(),
            "password" => Hash::make("123123123"),
            "is_admin" => true
        ]);

        User::create([
            "name" => "public-api",
            "email" => "support@linktonepro.site",
            'email_verified_at' => now(),
            "password" => Hash::make(md5(now())),
            "is_admin" => false
        ]);

        $user = User::find(2);
        $token = $user->createToken('api');
        $this->command->info('Public API Token '.$token->plainTextToken);

//        $contents = "NEXT_APP_URL=https://linkt.one\n"
//        ."NEXT_PUBLIC_BACKEND_URL=https://api.linktonepro.site\n"
//        ."NEXT_API_KEY=$token->plainTextToken\n";
//
//        $dir = storage_path("envs/public");
//        $filename = '.env';
//        if (!file_exists($dir)) {
//            mkdir($dir, 0777, true);
//        }
//        $path = "$dir/$filename";
//        file_put_contents($path, $contents);

        $this->call(DictionariesSeeder::class);
        $this->call(GeoSeeder::class);

        User::create([
            "name" => "Arrested",
            "lastname" => "Development",
            "email" => "arrested.development@ya.ru",
            'email_verified_at' => now(),
            "password" => Hash::make("123123123"),
            "phone" => '79199199191',
            "country" => 'Россия',
            "city" => 'Ростов-на-Дону'
        ]);


    }
}
