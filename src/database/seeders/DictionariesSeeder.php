<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class DictionariesSeeder extends Seeder
{

    protected $toTruncate = [
        'artist_types',
        'genres',
        'release_types',
        'video_types',
        'concert_types'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Schema::disableForeignKeyConstraints();

        foreach($this->toTruncate as $table) {
            DB::table($table)->truncate();
        }

        Schema::enableForeignKeyConstraints();

        DB::table('artist_types')->insert([
            ['name' => 'Группа'],
            ['name' => 'Вокалист'],
            ['name' => 'Исполнитель'],
            ['name' => 'Композитор'],
            ['name' => 'Битмейкер'],
            ['name' => 'DJ'],
        ]);

        DB::table('genres')->insert([
            ['name' => 'Поп-музыка'],
            ['name' => 'Электронная музыка'],
            ['name' => 'R&B'],
            ['name' => 'Рок'],
            ['name' => 'Блюз'],
            ['name' => 'Хип-хоп'],
            ['name' => 'Кантри'],
            ['name' => 'Регги'],
            ['name' => 'Ска'],
            ['name' => 'Джаз'],
            ['name' => 'Классическая музыка'],
            ['name' => 'Фолк-музыка'],
            ['name' => 'Латиноамериканская музыка'],
            ['name' => 'Шансон'],
            ['name' => 'Авторская песня'],
            ['name' => 'Романс'],
        ]);

        DB::table('release_types')->insert([
            ['name' => 'Альбом'],
            ['name' => 'Сингл/EP'],
            ['name' => 'Live'],
            ['name' => 'Remastering'],
            ['name' => 'Tribute'],
            ['name' => 'Remix']
        ]);

        DB::table('video_types')->insert([
            ['name' => 'Клип'],
            ['name' => 'Lyric Video'],
            ['name' => 'Live']
        ]);

        DB::table('concert_types')->insert([
            ['name' => 'Сольный концерт'],
            ['name' => 'Фестиваль'],
        ]);

        Model::reguard();
    }
}
