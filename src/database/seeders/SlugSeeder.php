<?php

namespace Database\Seeders;

use App\Models\Concert;
use App\Models\Release;
use App\Models\Slug;
use App\Models\Video;
use App\Services\SlugsService;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $releases = Release::all()->load('artist');
        foreach ($releases as $release) {
            (new SlugsService())->create($release->artist, $release, Str::slug($release->name));
        }

        $videos = Video::all()->load('artist');
        foreach ($videos as $video) {
            (new SlugsService())->create($video->artist, $video, Str::slug($video->name));
        }

        $concerts = Concert::all()->load('artist');
        foreach ($concerts as $concert) {
            $slug = (new Carbon($concert->start_datetime))->format("dmy");
            $slugCount = Slug::where(["artist_slug" => $concert->artist->slug, "slug" => $slug])->count();
//            $slugCount = Slug::where(["slug" => $slug])->count();

            //(new SlugsService())->create($concert->artist, $concert, $slug.'-'.md5(microtime()));
            (new SlugsService())->create($concert->artist, $concert, $slug);
        }
    }
}
