<x-layout>
    @foreach ($introLines as $line)
        <span style="font-size: 16px;margin-bottom: 30px; display: block;">{{ $line }}</span>
    @endforeach

    <div style="display: block;margin: 0 auto;margin-bottom: 30px;">
        <a href="{{ $actionUrl }}" target="_blank" style="text-align: center;border-radius: 4px;background-color: #0088CC;padding: 8px 14px;color: rgb(255, 255, 255);text-decoration: none;display: block;margin: 0px auto;max-width: 175px;border: 1px solid #0088CC;">
            {{ $actionText }}
        </a>
    </div>
    <span
        style="font-size: 16px;padding-bottom: 25px;border-bottom: 1px solid rgb(232, 229, 239);display: block;">С уважением,<br />Linktone</span>
    <p style=" font-size: 14px;">
        Если не получается нажать на кнопку, скопируйте и вставте в строку адреса браузера следующую ссылку:
        <a href="{{ $actionUrl }}" target="_blank" style="color: rgb(56, 105, 212);text-decoration: none;word-break: break-all">
            {{ $actionUrl }}
        </a>
    </p>

</x-layout>
