<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Linktone</title>
</head>

<body>

<div style="display: flex;justify-content: center;font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif,Segoe UI, Roboto, Helvetica, Arial,Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol"
     ;>
    <div style="width: 100%;max-width: 570px;padding: 24px 54px;background-color: #ecedf0;">
        <img src="{{env("APP_URL").'/logo.svg'}}" alt="" style="margin: 0 auto;display: block;margin-bottom: 25px;" />
        <div
            style="background-color: rgb(255, 255, 255); border-color: rgb(232, 229, 239); border-radius: 2px; border-width: 1px; box-shadow: rgba(0, 0, 150, 0.024) 0px 2px 0px, rgba(0, 0, 150, 0.016) 2px 4px 0px;padding: 24px;">
            <h1 style="color: rgb(0, 0, 0); font-size: 18px; font-weight: bold;">
                Здравствуйте!
            </h1>
            {{ $slot }}
        </div>
        <span style="display: block;text-align: center;padding: 32px;color: rgb(176, 173, 197); font-size: 12px;">© 2022 Linktone</span>
    </div>
</div>
</body>

</html>
