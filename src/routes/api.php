<?php

use App\Http\Controllers\AdminAPIController;
use App\Http\Controllers\AppAPIController;
use App\Http\Controllers\ArtistBannersController;
use App\Http\Controllers\ArtistsController;
use App\Http\Controllers\ArtistTypesController;
use App\Http\Controllers\ConcertController;
use App\Http\Controllers\ConcertTypeController;
use App\Http\Controllers\GenresController;
use App\Http\Controllers\ReleaseTypeController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\VideoTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ReleasesController;
use \App\Http\Controllers\TicketOfficeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('app')
    ->middleware(['auth:sanctum', 'verified'])
    ->controller(AppAPIController::class)
    ->group(function () {
        Route::get('/get', 'getAppData');
        Route::get('/slug/{slug}', [ArtistsController::class, 'getBySlug']);
        Route::get('/slug/{artist_slug}/{slug}', 'getBySlug');
        Route::get('/user', 'getUser');
        Route::post('/user', 'updateUser');
        Route::delete('/user', 'deleteUser');
        Route::post('/getUserByEmail', 'getUserByEmail');
        Route::post('/searchCountry', 'searchCountry');
        Route::post('/searchCity', 'searchCity');
    });

Route::prefix('admin')
    ->middleware(['auth:sanctum', 'checkAdmin'])
    ->controller(AdminAPIController::class)
    ->group(function () {
        Route::post('/searchUsers', 'searchUsers');
        Route::post('/userBan', 'userBan');
        Route::post('/userAdmin', 'userAdmin');
        Route::post('/searchArtists', 'searchArtists');
        Route::post('/banArtist', 'banArtist');
        Route::post('/banRelease', 'banRelease');
        Route::post('/banVideo', 'banVideo');
        Route::post('/banConcert', 'banConcert');
        Route::post('/searchReleases', 'searchReleases');
        Route::post('/searchVideos', 'searchVideos');
        Route::post('/sendTestEmail', 'sendTestEmail');
        Route::post('/createAPIToken', 'createAPIToken');
        Route::post('/revalidatePath', 'revalidatePath');

    });

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function (Request $request) {
//        if ($request->user()->is_banned)
//            return response(['message' => 'forbidden'], 403);
        return $request->user();
    });

    Route::apiResource('users', UsersController::class);

    Route::apiResource('artists', ArtistsController::class);
    Route::get('artists/slug/{slug}', [ArtistsController::class, 'getBySlug']);
    Route::post('artists/setBanned', [ArtistsController::class, 'setBanned']);
    Route::apiResource('artist_types', ArtistTypesController::class)->except([
        'show'
    ]);
    Route::post('artist_types/searchByName', [ArtistTypesController::class, 'searchByName']);

    Route::apiResource('artist_banners', ArtistBannersController::class)->except([
        'index', 'show'
    ]);
    Route::apiResource('genres', GenresController::class)->except([
        'show'
    ]);

    Route::apiResource('releases', ReleasesController::class);
    Route::post('releases/setBanned', [ReleasesController::class, 'setBanned']);
    Route::apiResource('release_types', ReleaseTypeController::class)->except([
        'show'
    ]);
    Route::post('release_types/searchByName', [ReleaseTypeController::class, 'searchByName']);
    Route::apiResource('stories', StoryController::class)->except([
        'index', 'show'
    ]);

    Route::apiResource('videos', VideoController::class);
    Route::post('videos/setBanned', [VideoController::class, 'setBanned']);
    Route::apiResource('video_types', VideoTypeController::class)->except([
        'show'
    ]);

    Route::apiResource('concerts', ConcertController::class);
    Route::post('concerts/setBanned', [ConcertController::class, 'setBanned']);
    Route::apiResource('concert_types', ConcertTypeController::class)->except([
        'show'
    ]);

    Route::apiResource('ticket_offices', TicketOfficeController::class);
});
