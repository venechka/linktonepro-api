<?php

namespace App\Services;

use App\Models\Artist;
use App\Models\Concert;
use App\Models\Release;
use App\Models\Slug;
use App\Models\Video;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

//use GuzzleHttp\Exception;

class SlugsService {
    public function sendRevalidateRequest($path) {
        $url = env('PUBLIC_FRONTEND_URL').'/api/revalidate';
        $client = new Client();
        try {
            $client->request('POST', $url, ['json' => [
                'secret' => env('PUBLIC_FRONTEND_SECRET'),
                'path' => $path
            ]]);
        } catch (Exception $exception) {
            //return response('resource was not revalidated');
            //TODO make revalidation job
            report($exception);
        }
    }

    public function create($artist, $model, $slug)
    {
        $modelClass = class_basename($model);

        Slug::insert([
            'artist_slug' => $artist->slug,
            'slug' => $slug,
            'model' => $modelClass,
            'model_id' => $model->id
        ]);
    }

    public function update($oldSlug, $newSlug)
    {
        if ($oldSlug === $newSlug)
            return;

        $oldSlug->slug = $newSlug;
        $oldSlug->save();
    }

    public function revalidate($model) {
        $modelClass = class_basename($model);

        if ($modelClass === 'Artist')
            $artist = $model;
        else
            $artist = Artist::find($model->artist_id);

        $releases = Release::where(['artist_id' => $artist->id])->get();
        $videos = Video::where(['artist_id' => $artist->id])->get();
        $concerts = Concert::where(['artist_id' => $artist->id])->get();

        $this->sendRevalidateRequest("/$artist->slug");
        foreach ($releases as $release)
            $this->sendRevalidateRequest("/$artist->slug/$release->slug");
        foreach ($videos as $video)
            $this->sendRevalidateRequest("/$artist->slug/$video->slug");
        foreach ($concerts as $concert)
            $this->sendRevalidateRequest("/$artist->slug/$concert->slug");

    }
}
