<?php

namespace App\Services;

use App\Models\File;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class FileService
{
    public $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    public function getRandomPicture($folder, $model, $width = 100, $height = 100, $keywords = '') {
        $baseUrl = "https://loremflickr.com"; //320/240
        $url = "$baseUrl/$width/$height/$keywords";
        $filename = md5(microtime()).'.jpg';

        $filepath = storage_path("app/public/$folder");
        if(!\Illuminate\Support\Facades\File::exists($filepath)){
            \Illuminate\Support\Facades\File::makeDirectory($filepath);
        }

        $this->client->request('GET', $url, ['sink' => $filepath.'/'.$filename]);
        $file = new \Illuminate\Http\File($filepath.'/'.$filename);

        return $this->storeAndRegister($file, $filename, $folder, $model);
    }

    public function storeAndRegister($_file, $filename, $folder = "", $model = "") {
        $file = new File();
        $file->model = $model;
        $file->original_name = $filename;
        $file->path = Storage::put($folder, $_file);
        $file->save();
        return $file->id;
    }

    public function removeAndDelete($id) {
        $file = File::find($id);
        if ($file) {
            Storage::delete($file->path);
            $file->delete();
        }
    }
}
