<?php

namespace App\Services;

class HelpersService {
    static function isEqual ($arr) {
        if (count($arr) <= 1)
            return true;

        $isEquals = true;
        $prevItem = $arr[0];
        for ($i = 1; $i < count($arr); $i++) {
            $isEquals = $prevItem === $arr[$i];
            if (!$isEquals) break;
            $prevItem = $arr[$i];
        }

        return $isEquals;
    }
}
