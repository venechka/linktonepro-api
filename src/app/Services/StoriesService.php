<?php

namespace App\Services;


use App\Models\Story;

class StoriesService {
    /**
     * @throws \ErrorException
     */
    public function store($model, $files)
    {
        if (count($files) > 3)
            throw new \ErrorException("Stories count mismatch");

        $fileService = new FileService();
        $field = strtolower(class_basename($model)).'_id';

        foreach ($files as $file)
        {
            $storyModel = new Story();
            $storyModel->$field = $model->id;
            $storyModel->uploaded_by_user_id = auth()->user()->id;
            $storyModel->file_id = $fileService->storeAndRegister(
                $file,
                $file->getClientOriginalName(),
                'stories',
                'Story'
            );
            $storyModel->save();
        }
    }

    private function countStories($model, $stories) {
        $field = strtolower(class_basename($model)).'_id';

        $overall = Story::where($field, $model->id)->count();
        $new = 0;
        $delete = 0;

        foreach ($stories as $story) {
            if ($story->id === 'new')
                $new++;
            elseif (property_exists($story, "delete") && $story->delete)
                $delete++;
        }

        $overall = $overall + $new - $delete;

        return $overall;
    }


    /**
     * @throws \ErrorException
     */
    public function update($model, $stories, $files)
    {
        if ($this->countStories($model, $stories) > 3)
            throw new \ErrorException("Stories count mismatch");

        $fileService = new FileService();
        $field = strtolower(class_basename($model)).'_id';

        foreach ($stories as $story) {
            if ($story->id === 'new') {
                $storyModel = new Story();
                $storyModel->$field = $model->id;
                $storyModel->uploaded_by_user_id = auth()->user()->id;
                $storyModel->file_id = $fileService->storeAndRegister(
                    $files[$story->file_index],
                    $files[$story->file_index]->getClientOriginalName(),
                    'stories',
                    'Story'
                );
                $storyModel->save();
            } elseif (property_exists($story, "delete") && $story->delete) {
                $storyModel = Story::find($story->id);
                if (!$storyModel)
                    throw new \ErrorException('Story ID:'.$story->id.' not found');

                $fileService->removeAndDelete($storyModel->file_id);
                $storyModel->delete();
            } else {
                $storyModel = Story::find($story->id);
                if (!$storyModel)
                    throw new \ErrorException('Story ID:'.$story->id.' not found');

                $storyModel->uploaded_by_user_id = auth()->user()->id;
                if ($story->file_index >= 0) {
                    $fileService->removeAndDelete($storyModel->file_id);
                    $storyModel->file_id = $fileService->storeAndRegister(
                        $files[$story->file_index],
                        $files[$story->file_index]->getClientOriginalName(),
                        'stories',
                        'Story'
                    );
                }
                $storyModel->save();
            }
        }
    }
}
