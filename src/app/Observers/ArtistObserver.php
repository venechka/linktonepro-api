<?php

namespace App\Observers;

use App\Http\Traits\RevalidationTrait;
use App\Models\Artist;

class ArtistObserver
{
    use RevalidationTrait;
}
