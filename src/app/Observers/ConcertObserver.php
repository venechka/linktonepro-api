<?php

namespace App\Observers;

use App\Http\Traits\RevalidationTrait;

class ConcertObserver
{
    use RevalidationTrait;
}
