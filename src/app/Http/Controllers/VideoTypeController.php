<?php

namespace App\Http\Controllers;

use App\Models\VideoType;
use Illuminate\Http\Request;

class VideoTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VideoType::orderBy('order', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', VideoType::class);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $videoType = new VideoType();
        $videoType->name = $request->name;
        $videoType->order = $request->order;
        $videoType->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VideoType  $videoType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VideoType $videoType)
    {
        $this->authorize('update', $videoType);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $videoType->name = $request->name;
        $videoType->order = $request->order;
        $videoType->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VideoType  $videoType
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoType $videoType)
    {
        $this->authorize('delete', $videoType);

        $videoType->delete();
    }
}
