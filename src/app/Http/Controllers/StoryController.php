<?php

namespace App\Http\Controllers;

use App\Models\Story;
use App\Services\FileService;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO check 3 banners max per artist
        $this->validate($request, [
            "release_id" => ['required_without_all:video_id,concert_id', 'exists:releases,id'],
            "video_id" => ['required_without_all:release_id,concert_id', 'exists:videos,id'],
            "concert_id" => ['required_without_all:release_id,video_id', 'exists:concerts,id'],
            "story" => ['required', 'file', 'mimes:mp4', 'max:15360']
        ]);

//        $this->authorize('create', [Story::class, $request->get('release_id'),  $request->get('video_id'),  $request->get('concert_id')]);

        $story = new Story();
        if ($request->has('release_id'))
            $story->release_id = $request->release_id;
        if ($request->has('video_id'))
            $story->video_id = $request->video_id;
        if ($request->has('concert_id'))
            $story->concert_id = $request->concert_id;

        $this->authorize('create', $story);

        $story->uploaded_by_user_id = auth()->user()->id;
        $story->file_id = (new FileService())->storeAndRegister($request->file("story"), $request->file('story')->getClientOriginalName(), "stories", "Story");
        $story->save();

        return ['story_id' => $story->id];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Story $story)
    {
        $this->validate($request, [
            "story" => ['required', 'file', 'mimes:mp4', 'max:15360']
        ]);

        $this->authorize('update', $story);

        $story->uploaded_by_user_id = auth()->user()->id;
        (new FileService())->removeAndDelete($story->file_id);
        $story->file_id = (new FileService())->storeAndRegister($request->file("story"), $request->file('story')->getClientOriginalName(), "stories", "Story");
        $story->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        $this->authorize('delete', $story);

        (new FileService())->removeAndDelete($story->file_id);
        $story->delete();
    }
}
