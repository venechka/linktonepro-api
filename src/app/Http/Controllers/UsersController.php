<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::orderBy('id', 'asc')->get();
//        if (auth()->user()->is_admin)
//            return User::orderBy('id', 'asc')
//                ->paginate(50)->withPath($request->base_url ?? env('ADMIN_FRONTEND_URL'));
//        else
//            return User::where('is_banned', false)
//                ->orderBy('name', 'asc')
//                ->paginate(20)->withPath($request->base_url ?? env('FRONTEND_URL'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $fields = $request->all();
        $fields['password'] = Hash::make($fields['password']);
        User::create($fields);
        //(new ProfilesService())->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $request->validate([
            'name' => ['string', 'max:255'],
            "is_banned" => ['boolean'],
            "is_admin" => ['boolean']
        ]);

        if ($request->get('email') != $user->email)
            $this->validate($request, [
                'email' => ['email', 'max:255', 'unique:users,email'],
            ]);

        $fields = $request->all();

        if ($request->get('password')) {
            $this->validate($request, [
                'password' => ['confirmed', Rules\Password::defaults()],
            ]);
            $fields['password'] = Hash::make($request->password);
        }

        $user->update($fields);

        if ($request->has('picture')) {
            $fileService = new FileService();

            if ($request->get('picture') === null && $request->file('picture') === null) {
                $fileService->removeAndDelete($user->picture_file_id);
                $user->picture_file_id = null;
                $user->save();
            } else {
                $this->validate($request, [
                    'picture' => ['file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
                ]);
                $fileService->removeAndDelete($user->picture_file_id);
                $user->picture_file_id = $fileService->storeAndRegister($request->file('picture'), 'user_pictures', "User");
            }
            $user->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();
    }
}
