<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistBanner;
use App\Models\ArtistManager;
use App\Models\Concert;
use App\Models\Release;
use App\Models\Story;
use App\Models\Video;
use App\Models\Slug as SlugModel;
use App\Rules\Slug;
use App\Services\FileService;
use App\Services\HelpersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ArtistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->has('q'))
            $where = [['name', 'like', '%' . $request->q . '%']];
        if (auth()->user()->is_admin)
            return Artist::with('artist_type', 'genre')
                ->where($where)
                ->orderBy('id', 'asc')
                ->paginate(20)->withPath($request->base_url ?? env('ADMIN_FRONTEND_URL'));
        else
            return Artist::with('artist_type', 'genre')
                ->orderBy('name', 'asc')
                ->where('is_banned', false)
                ->paginate(20)->withPath($request->base_url ?? env('FRONTEND_URL'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'owner_id' => ['required', 'exists:users,id'],
            'slug' => ['required', 'string', 'unique:artists,slug'],
            'name' => ['required', 'string'],
            'artist_types_id' => ['required', 'exists:artist_types,id'],
            'genres_id' => ['required', 'exists:genres,id'],
            'country' => ['required', 'string'],
            'city' => ['required', 'string'],
            'logo' => ['mimes:jpg,jpeg,png,bmp|max:10240'],
            'picture' => ['mimes:jpg,jpeg,png,bmp|max:10240'],
            'banners' => ['json'],
            'banners_pictures' => ['array', 'max:3'],
            'banners_pictures.*' => ['mimes:jpg,jpeg,png,bmp|max:10240']
        ]);

        $artist = new Artist($request->all());

        $fileService = new FileService();

        if ($request->has('logo')) {
            $artist->logo_file_id = $fileService->storeAndRegister($request->file('logo'), $request->file('logo')->getClientOriginalName(), 'artist_pictures', "Artist");
        }

        if ($request->has('picture')) {
            $artist->picture_file_id = $fileService->storeAndRegister($request->file('picture'), 'artist_pictures', "Artist");
        }

        $artist->save();

        if ($request->has('banners')) {
            $banners = json_decode($request->get('banners'));

            if (count($banners) <= 3) {
                DB::transaction(function () use ($fileService, $artist, $banners, $request) {
                    foreach ($banners as $banner) {
                        $bannerModel = new ArtistBanner;
                        $bannerModel->artist_id = $artist->id;
                        $bannerModel->uploaded_by_user_id = auth()->user()->id;
                        $bannerModel->name = $banner->name;
                        $bannerModel->link = $banner->link;
                        $bannerModel->file_id = $fileService->storeAndRegister(
                            $request->file('banners_pictures')[$banner->picture_index],
                            $request->file('banners_pictures')[$banner->picture_index]->getClientOriginalName(),
                            'artist_banners',
                            'ArtistBanner'
                        );
                        $bannerModel->save();
                    }
                });
            } else {
                return response(['message' => 'Banners count mismatch'], 422);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Artist $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    {
//        if (!auth()->user()->is_admin && $artist->is_banned)
//            return response(["message" => "The artist is banned"], 403);

        return $artist->load(['managers.user' => function ($query) {
            $query->select('id', 'name', 'email');
        }])->load('banners.file', 'owner', 'artist_type', 'genre', 'releases', 'releases.release_type', 'releases.stories', 'videos', 'concerts');
//        return $artist->load('owner', 'artist_type', 'genre');
    }

    public function getBySlug($slug)
    {
        $artist = Artist::where('slug', $slug)->first();
        if (!$artist)
            return response(['message' => "Artist not found"], 404);

        if (!auth()->user()->is_admin && $artist->is_banned)
            return response(["message" => "The artist is banned"], 403);

        return $artist->load('banners.file', 'owner', 'artist_type', 'genre', 'releases', 'releases.release_type', 'releases.stories', 'videos', 'videos.video_type', 'concerts', 'concerts.concert_type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Artist $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
//        if (!auth()->user()->is_admin && $artist->is_banned)
//            return response(["message" => "The artist is banned"], 403);

        $this->validate($request, [
            'owner_id' => ['exists:users,id'],
            'artist_types_id' => ['exists:artist_types,id'],
            'genres_id' => ['exists:genres,id'],
            'country' => ['string'],
            'city' => ['string'],
            'description' => ['nullable', 'string'],
            'members' => ['json'],
            'banners' => ['json'],
            'banners_pictures' => ['array', 'max:3'],
            'banners_pictures.*' => ['mimes:jpg,jpeg,png,bmp|max:10240']
        ]);

        $this->authorize('update', $artist);

        DB::transaction(function () use ($artist, $request) {

            if ($request->get('slug') && $request->get('slug') !== $artist->slug) {
                $this->validate($request, [
                    'slug' => ['string', new Slug(), 'unique:artists,slug'],
                ]);
                SlugModel::where('artist_slug', $artist->slug)->update(['artist_slug' => $request->get('slug')]);
                //TODO revalidate old page on front so it will become 404
            }

            $artist->update($request->all());

            $fileService = new FileService();

            if ($request->has('logo')) {
                if ($request->get('logo') === null && $request->file('logo') === null) {
                    $fileService->removeAndDelete($artist->logo_file_id);
                    $artist->logo_file_id = null;
                } else {
                    $this->validate($request, [
                        'logo' => ['file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
                    ]);
                    $fileService->removeAndDelete($artist->logo_file_id);
                    $artist->logo_file_id = (new FileService)->storeAndRegister($request->file('logo'), $request->file('logo')->getClientOriginalName(), 'artist_pictures', "Artist");

                }
            }

            if ($request->has('picture')) {
                if ($request->get('picture') === null && $request->file('picture') === null) {
                    $fileService->removeAndDelete($artist->picture_file_id);
                    $artist->picture_file_id = null;

                } else {
                    $this->validate($request, [
                        'picture' => ['file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
                    ]);
                    $fileService->removeAndDelete($artist->picture_file_id);
                    $artist->picture_file_id = (new FileService)->storeAndRegister($request->file('picture'), $request->file('picture')->getClientOriginalName(), 'artist_pictures', "Artist");
                }
            }

            if ($request->has('members')) {
                $artist->members = $request->get('members');
            }

            if ($request->has('managers')) {
                $records = array_map(function ($value) use ($artist) {
                    return ['artist_id' => $artist->id, 'user_id' => $value];
                }, json_decode($request->managers));


                ArtistManager::where('artist_id', $artist->id)->delete();
                if (count($records))
                    ArtistManager::insert($records);
            }

            $artist->save();

            if ($request->has('banners')) {
                $banners = json_decode($request->get('banners'));
                if ($this->countBanners($artist->id, $banners) <= 3) {

                    foreach ($banners as $banner) {
                        if ($banner->id === 'new') {
                            $bannerModel = new ArtistBanner;
                            $bannerModel->artist_id = $artist->id;
                            $bannerModel->uploaded_by_user_id = auth()->user()->id;
                            $bannerModel->name = $banner->name;
                            $bannerModel->link = $banner->link;
                            $bannerModel->file_id = $fileService->storeAndRegister(
                                $request->file('banners_pictures')[$banner->picture_index],
                                $request->file('banners_pictures')[$banner->picture_index]->getClientOriginalName(),
                                'artist_banners',
                                'ArtistBanner'
                            );
                            $bannerModel->save();
                        } elseif (property_exists($banner, "delete") && $banner->delete) {
                            $bannerModel = ArtistBanner::find($banner->id);
                            if (!$bannerModel)
                                return response(['message' => 'Banner ID:' . $banner->id . ' not found'], 422);

                            $fileService->removeAndDelete($bannerModel->file_id);
                            $bannerModel->delete();
                        } else {
                            $bannerModel = ArtistBanner::find($banner->id);
                            if (!$bannerModel)
                                return response(['message' => 'Banner ID:' . $banner->id . ' not found'], 422);

                            $bannerModel->uploaded_by_user_id = auth()->user()->id;
                            $bannerModel->name = $banner->name;
                            $bannerModel->link = $banner->link;
                            if ($banner->picture_index >= 0) {
                                $fileService->removeAndDelete($bannerModel->file_id);
                                $bannerModel->file_id = $fileService->storeAndRegister(
                                    $request->file('banners_pictures')[$banner->picture_index],
                                    $request->file('banners_pictures')[$banner->picture_index]->getClientOriginalName(),
                                    'artist_banners',
                                    'ArtistBanner'
                                );
                            }
                            $bannerModel->save();
                        }
                    }

                } else {
                    return response(['message' => 'Banners count mismatch'], 422);
                }
            }
        });
    }

    private function countBanners($artistId, $banners)
    {
        $overall = ArtistBanner::where('artist_id', $artistId)->count();
        $new = 0;
        $delete = 0;

        foreach ($banners as $banner) {
            if ($banner->id === 'new')
                $new++;
            elseif (property_exists($banner, "delete") && $banner->delete)
                $delete++;
        }

        $overall = $overall + $new - $delete;

        return $overall;
    }

    private function checkUploadingBannerPictures($banners, $pictures)
    {

    }

//    public function uploadBanner(Request $request, Artist $artist)
//    {
//        $this->validate($request, [
//           "banner" => ['required', 'file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
//        ]);
//
//        $artistBanner = new ArtistBanner();
//        $artistBanner->artist_id = $artist->id;
//        $artistBanner->user_id = auth()->user()->id;
//        $artistBanner->file = (new FileService())->storeAndRegister($request->file("banner"));
//        $artistBanner->save();
//    }

//    public function setBanned(Request $request)
//    {
//        $this->validate($request, [
//            "id" => ['required', 'exists:artists,id'],
//            "banned" => ['required', 'boolean']
//        ]);
//
//        $artist = Artist::find($request->id);
//        $artist->banned = $request->banned;
//        $artist->save();
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Artist $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        //TODO move to scheduled job
        /*
        DB::transaction(function () use ($artist) {
            //deleting files
            $releases = Release::where('artist_id', $artist->id)->select('id')->get()->toArray();

            $videos = Video::where('artist_id', $artist->id)->select('id')->get()->toArray();
            $releaseIDs = array_map(function ($value) {
                return $value['id'];
            }, $releases);
            $videoIDs = array_map(function ($value) {
                return $value['id'];
            }, $videos);

            Story::whereIn('release_id', function ($query) use ($artist) {
                $query->select('id')
                    ->from('releases')
                    ->where('artist_id', $artist->id);
            })->orWhereIn('video_id', function ($query) use ($artist) {
                $query->select('id')
                    ->from(with(new Video)->getTable())
                    ->where('artist_id', $artist->id);
            })->orWhereIn('concert_id', function ($query) use ($artist) {
                $query->select('id')
                    ->from(with(new Concert)->getTable())
                    ->where('artist_id', $artist->id);
            })->delete();

            Release::where('artist_id', $artist->id)->delete();
            Video::where('artist_id', $artist->id)->delete();
            Concert::where('artist_id', $artist->id)->delete();
            ArtistManager::where('artist_id', $artist->id)->delete();
            $artist->delete();
        });*/
        $this->authorize('delete', $artist);

        $artist->delete();
    }
}
