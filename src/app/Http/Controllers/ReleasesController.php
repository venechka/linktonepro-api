<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\RelatedVideo;
use App\Models\Release;
use App\Models\Story;
use App\Models\Video;
use App\Models\Slug as SlugModel;
use App\Rules\Slug;
use App\Rules\SlugExists;
use App\Services\FileService;
use App\Services\SlugsService;
use App\Services\StoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ReleasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->has('q'))
            $where = [['name', 'like', '%'.$request->q.'%']];

        return Release::with('release_type')->orderBy('id', 'asc')
            ->where($where)
            ->paginate(20)->withPath($request->base_url ?? env('ADMIN_FRONTEND_URL'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->has('artist_id'))
            return response(["errors" => ["artist_id" => ["Поле artist_id обязательно для заполнения."]]], 422);

        $artist = Artist::find($request->get('artist_id'));

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $this->authorize('create', [Release::class, $request->artist_id]);

        $this->validate($request, [
            'name' => ['required', 'string'],
            'slug' => ['required', 'string', new Slug(), new SlugExists($artist->slug)],
            'cover' => ['required', 'file', 'mimes:jpg,jpeg,png,bmp', 'max:10240'],
            'videos' => ['json'],
            'release_type_id' => ['required', 'exists:release_types,id'],
            'description' => ['nullable', 'string'],
            'publish_date' => ['required', 'date'],
            'label_company' => ['required', 'string'],
            'tracks' => ['json'],
            'tags' => ['json'],
            'social_links' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ]);

        try {
            DB::transaction(function () use ($artist, $request) {

                $release = new Release($request->all());

                if ($request->has('cover'))
                    $release->cover_file_id = (new FileService())->storeAndRegister($request->file('cover'), $request->file('cover')->getClientOriginalName(), 'release_covers', 'Release');

                $release->save();

                (new SlugsService())->create($artist, $release, $request->slug);

                if ($request->has('videos')) {
                    $videos = json_decode($request->videos);

                    foreach ($videos as $videoId) {
                        $video = Video::find($videoId);
                        $video->release_id = $release->id;
                        $video->save();
                    }
                }

                if ($request->has('stories_files')) {
                    (new StoriesService())->store($release, $request->file('stories_files'));
                }
            });
        } catch (\ErrorException $exception) {
            //dd($exception->getMessage());
            return response(['message' => $exception->getMessage()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Release  $release
     * @return \Illuminate\Http\Response
     */
    public function show(Release $release)
    {
        return $release->load('related_videos.video.video_type', 'stories', 'release_type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Release  $release
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Release $release)
    {
        $this->authorize('update', $release);

//        if ($request->has('artist_id'))
//            return response(["errors" => ["artist_id" => ["Поле artist_id обязательно для заполнения."]]], 422);

        $artist = Artist::find($release->artist_id);

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $validationRules = [
            'name' => ['nullable', 'string'],
            //'artist_id' => ['exists:artists,id'], TODO перемещение релиза между артистами
            'release_type_id' => ['exists:release_types,id'],
            'cover' => ['file', 'mimes:jpg,jpeg,png,bmp', 'max:10240'],
            'description' => ['nullable', 'string'],
            'publish_date' => ['date'],
            'label_company' => ['string'],
            'tracks' => ['json'],
            'tags' => ['json'],
            'links' => ['json'],
            'related_videos' => ['json'],
            'stories' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ];

        $oldSlug = SlugModel::where(['model' => 'Release', 'model_id' => $release->id])->first();
        if (!$oldSlug || ($request->has('slug') && $oldSlug['slug'] !== $request->get('slug')))
            $validationRules['slug'] = ['string', new Slug(), new SlugExists($artist->slug)];

        $this->validate($request, $validationRules);

        try {
            DB::transaction(function () use ($request, $oldSlug, $artist, $release) {
                $release->update($request->all());

                if ($request->has('cover')) {
                    $release->cover_file_id = (new FileService)->storeAndRegister($request->file('cover'), $request->file('cover')->getClientOriginalName(), 'release_covers', "Release");
                    $release->save();
                }

                if ($request->has('related_videos')) {
                    $records = array_map(function ($value) use ($release) {
                        return ['release_id' => $release->id, 'video_id' => $value];
                    }, json_decode($request->related_videos));

                    RelatedVideo::where('release_id', $release->id)->delete();

                    if (count($records))
                        RelatedVideo::insert($records);

                }

                if ($request->has('stories')) {
                    $stories = json_decode($request->get('stories'));
                    (new StoriesService())->update($release, $stories, $request->file('stories_files'));
                }

                if ($request->has('slug')) {
                    if ($oldSlug)
                        (new SlugsService())->update($oldSlug, $request->slug);
                    else
                        (new SlugsService())->create($artist, $release, $request->slug);
                }
            });
        } catch (\ErrorException $exception) {
            return response(['message' => $exception->getMessage()], 422);
        }
    }

//    public function setBanned(Request $request)
//    {
//        $this->validate($request, [
//            "id" => ['required', 'exists:releases,id'],
//            "banned" => ['required', 'boolean']
//        ]);
//
//        $release = Release::find($request->id);
//        $release->banned = $request->banned;
//        $release->save();
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Release  $release
     * @return \Illuminate\Http\Response
     */
    public function destroy(Release $release)
    {
        $this->authorize('delete', $release);

        DB::transaction(function () use ($release) {
            //Deleting video relations
            RelatedVideo::where(['release_id' => $release->id])->delete();
            //Deleting slug
            SlugModel::where(['model' => 'release', 'model_id' => $release->id])->delete();
            //Delete cover file from storage
            (new FileService())->removeAndDelete($release->cover_file_id);
            $release->delete();
        });
    }
}
