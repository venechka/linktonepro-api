<?php

namespace App\Http\Controllers;

use App\Models\ReleaseType;
use Illuminate\Http\Request;

class ReleaseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ReleaseType::orderBy('order', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', ReleaseType::class);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $artistType = new ReleaseType();
        $artistType->name = $request->name;
        $artistType->order = $request->order;
        $artistType->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReleaseType  $releaseType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReleaseType $releaseType)
    {
        $this->authorize('update', $releaseType);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $releaseType->name = $request->name;
        $releaseType->order = $request->order;
        $releaseType->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReleaseType  $releaseType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReleaseType $releaseType)
    {
        $this->authorize('delete', $releaseType);

        $releaseType->delete();
    }
}
