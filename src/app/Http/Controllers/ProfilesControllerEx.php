<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\ProfilesService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Storage;

class ProfilesControllerEx extends Controller
{
    function get(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id'
        ]);

        return (new ProfilesService())->get($request->user_id);
    }

    function getAll()
    {
        return (new ProfilesService())->getAll();
    }

    function create(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        (new ProfilesService())->create($request->all());
    }

    function update(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'exists:users,id'],
        ]);

        $user = User::find($request->id);

        if ($request->get('email') != $user->email)
            $this->validate($request, [
                'email' => ['unique:users,email'],
            ]);

        $fields = $request->all();
        unset($fields['created_at']);
        unset($fields['updated_at']);
        if (!$request->get('password'))
            unset($fields['password']);
        unset($fields['password_confirmation']);

        (new ProfilesService())->update($fields);
    }

    function updatePicture(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        if ($request->get('picture') !== 'null') {
            $this->validate($request, [
                'picture' => ['required', 'file', 'max:1024']
            ]);

            (new ProfilesService())->updatePicture($request->user_id, $request->file('picture'));
        } else {
            (new ProfilesService())->deletePicture($request->user_id);
        }
    }

    function delete(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'exists:users,id']
        ]);

        (new ProfilesService())->delete($request->id);
    }

    function searchByName(Request $request)
    {
        $this->validate($request, [
            'query' => ['required']
        ]);

        return (new ProfilesService())->searchByName(trim($request->get('query')));
    }
}
