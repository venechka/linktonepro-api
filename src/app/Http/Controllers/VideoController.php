<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Slug as SlugModel;
use App\Models\Video;
use App\Rules\Slug;
use App\Rules\SlugExists;
use App\Services\SlugsService;
use App\Services\StoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->has('q'))
            $where = [['name', 'like', '%'.$request->q.'%']];

        return Video::with('artist', 'video_type')->orderBy('id', 'asc')
            ->where($where)
            ->paginate(20)->withPath($request->base_url ?? env('ADMIN_FRONTEND_URL'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->has('artist_id'))
            return response(["errors" => ["artist_id" => ["Поле artist_id обязательно для заполнения."]]], 422);

        $artist = Artist::find($request->get('artist_id'));

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $this->authorize('create', [Video::class, $request->artist_id]);

        $this->validate($request, [
            'name' => ['required', 'string'],
            'slug' => ['required', 'string', new Slug(), new SlugExists($artist->slug)],
            'link' => ['required', 'string'],
            'video_type_id' => ['required', 'exists:video_types,id'],
            'publish_date' => ['required', 'date'],
            'artist_id' => ['required', 'exists:artists,id'],
            'release_id' => ['exists:releases,id'],
            'description' => ['nullable', 'string'],
            'tags' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ]);

        DB::transaction(function () use ($artist, $request) {

            $video = new Video($request->all());
            $video->save();

            if ($request->has('stories_files')) {
                (new StoriesService())->store($video, $request->file('stories_files'));
            }

            (new SlugsService())->create($artist, $video, $request->slug);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return $video->load('artist', 'release', 'stories', 'video_type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $this->authorize('update', $video);

        $artist = Artist::find($video->artist_id);

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $validationRules = [
            'name' => ['string'],
            'link' => ['string'],
            'video_type_id' => ['exists:video_types,id'],
            'publish_date' => ['date'],
            'release_id' => ['nullable', 'exists:releases,id'],
            'description' => ['nullable', 'string'],
            'tags' => ['json'],
            'stories' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ];

        $oldSlug = SlugModel::where(['model' => 'Video', 'model_id' => $video->id])->first();
        if (!$oldSlug || ($request->has('slug') && $oldSlug['slug'] !== $request->get('slug')))
            $validationRules['slug'] = ['string', new Slug(), new SlugExists($artist->slug)];

        $this->validate($request, $validationRules);

        DB::transaction(function () use ($request, $oldSlug, $artist, $video) {
            $video->update($request->all());

            if ($request->has('stories')) {
                $stories = json_decode($request->get('stories'));
                (new StoriesService())->update($video, $stories, $request->file('stories_files'));
            }

            if ($request->has('slug')) {
                if ($oldSlug)
                    (new SlugsService())->update($oldSlug, $request->slug);
                else
                    (new SlugsService())->create($artist, $video, $request->slug);
            }
        });
    }

//    public function setBanned(Request $request)
//    {
//        $this->validate($request, [
//            "id" => ['required', 'exists:video,id'],
//            "banned" => ['required', 'boolean']
//        ]);
//
//        $video = Video::find($request->id);
//        $video->banned = $request->banned;
//        $video->save();
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $this->authorize('delete', $video);

        DB::transaction(function () use ($video) {
            $slug = SlugModel::where(['model' => 'Video', 'model_id' => $video->id])->first();
            $slug->delete();
            $video->delete();
        });
    }
}
