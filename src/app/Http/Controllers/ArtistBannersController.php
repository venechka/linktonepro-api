<?php

namespace App\Http\Controllers;

use App\Models\ArtistBanner;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ArtistBannersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO check 3 banners max per artist
        $this->validate($request, [
            "artist_id" => ['required', 'exists:artists,id'],
            "banner" => ['required', 'file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
        ]);

        $this->authorize('create', [ArtistBanner::class, $request->artist_id]);

        $artistBanner = new ArtistBanner();
        $artistBanner->artist_id = $request->artist_id;
        $artistBanner->uploaded_by_user_id = auth()->user()->id;
        $artistBanner->file_id = (new FileService())->storeAndRegister($request->file("banner"), $request->file('banner')->getClientOriginalName(), "artist_banners", "ArtistBanner");
        $artistBanner->save();

        return ['banner_id' => $artistBanner->id];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtistBanner $artistBanner)
    {
        $this->validate($request, [
            "banner" => ['required', 'file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
        ]);

        $this->authorize('update', $artistBanner);

        $artistBanner->uploaded_by_user_id = auth()->user()->id;
        (new FileService())->removeAndDelete($artistBanner->file_id);
        $artistBanner->file_id = (new FileService())->storeAndRegister($request->file("banner"), $request->file('banner')->getClientOriginalName(), "artist_banners", "ArtistBanner");
        $artistBanner->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtistBanner $artistBanner)
    {
        $this->authorize('delete', $artistBanner);

        (new FileService())->removeAndDelete($artistBanner->file_id);
        $artistBanner->delete();
    }
}
