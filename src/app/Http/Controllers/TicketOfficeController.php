<?php

namespace App\Http\Controllers;

use App\Models\TicketOffice;
use Illuminate\Http\Request;

class TicketOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TicketOffice::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string'],
            'domain' => ['required', 'url'],
            'suffix' => ['string', 'nullable'],
            'prefix' => ['string', 'nullable'],
            'referal' => ['nullable', 'url']
        ]);

        $ticketOffice = new TicketOffice($request->all());
        $ticketOffice->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TicketOffice  $ticketOffice
     * @return \Illuminate\Http\Response
     */
    public function show(TicketOffice $ticketOffice)
    {
        return $ticketOffice;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  \App\Models\TicketOffice  $ticketOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TicketOffice $ticketOffice)
    {
        $this->validate($request, [
            'name' => ['string'],
            'domain' => ['url'],
            'suffix' => ['string', 'nullable'],
            'prefix' => ['string', 'nullable'],
            'referal' => ['url', 'nullable']
        ]);

        $this->authorize('update', $ticketOffice);

        $ticketOffice->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TicketOffice  $ticketOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy(TicketOffice $ticketOffice)
    {
        $this->authorize('delete', $ticketOffice);

        $ticketOffice->delete();
    }
}
