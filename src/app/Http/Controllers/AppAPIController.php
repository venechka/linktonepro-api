<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistType;
use App\Models\Concert;
use App\Models\ConcertType;
use App\Models\Genre;
use App\Models\Release;
use App\Models\ReleaseType;
use App\Models\TicketOffice;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoType;
use App\Models\Slug as SlugModel;
use App\Policies\DictionaryPolicy;
use App\Rules\Slug;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;

class AppAPIController extends Controller
{
    public function getAppData(Request $request)
    {
        return [
            'user' => $request->user()->load('artists', 'managed_artists.artist'),
            'dictionaries' => [
                "genres" => Genre::all(),
                "artist_types" => ArtistType::all(),
                "release_types" => ReleaseType::all(),
                "video_types" => VideoType::all(),
                "concert_types" => ConcertType::all()
            ]
        ];
    }

    public function getBySlug($artist_slug, $slug)
    {
//        $this->validate($request, [
//            'artist_slug' => ['required', 'string', new Slug(), 'exists:artists,slug'],
//            'slug' => ['string', 'nullable']
//        ]);

        $artist = Artist::where('slug', $artist_slug)
            ->with('banners.file', 'owner', 'artist_type', 'genre', 'releases', 'releases.release_type', 'videos', 'videos.video_type', 'concerts', 'concerts.concert_type')
            ->first();

        if (!$artist)
            return response('Artist not found', 404);

        if ($artist->is_banned)
            return response('Artist is banned', 403);

        $slug = SlugModel::where(['artist_slug' => $artist->slug, 'slug' => $slug])->first();

        if ($slug) {
            $artist->model = $slug->model;
            switch ($slug->model) {
                case 'Release':
                    $artist->release = Release::find($slug->model_id)->load('release_type', 'related_videos.video.video_type', 'stories');
                    break;
                case 'Video':
                    $artist->video = Video::find($slug->model_id)->load('video_type', 'stories', 'release', 'related_releases.release.release_type');
                    break;
                case 'Concert':
                    $artist->concert = Concert::find($slug->model_id)->load('concert_type', 'stories');

                    break;
            }
        }
        return $artist;
    }

    public function getUser(Request $request)
    {
        return $request->user()->load('artists', 'managed_artists.artist');
    }

    public function updateUser(Request $request)
    {
        $user = $request->user();

        $validationRules = [
            'name' => ['string', 'max:255'],
            'password' => ['confirmed', Rules\Password::defaults()],
            'picture' => ['file', 'max:10240'],
            'delete_picture' => ['boolean']
        ];

        if ($request->get('email') !== $user->email)
            $validationRules['email'] = ['email', 'max:255', 'unique:users,email'];

        $request->validate($validationRules);

        if ($request->get('password')) {
            $request->merge(['password' => Hash::make($request->password)]);
            $request->request->remove('password_confirmation');
        }

        if ($request->has('picture')) {
            $request->merge(['picture_file_id' => (new FileService)->storeAndRegister($request->file('picture'), $request->file('picture')->getClientOriginalName(), 'user_pictures', "User")]);
            $request->request->remove('delete_picture');
        }

        if ($request->get('delete_picture') && $user->picture_file_id) {
            (new FileService)->removeAndDelete($user->picture_file_id);
            $request->merge(['picture_file_id' => null]);
        }

        $user->update($request->all());
    }

    public function deleteUser(Request $request) {
        $user = $request->user();
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $user->delete();
    }

    public function getUserByEmail(Request $request) {
        $this->validate($request, [
            'email' => ['email']
        ]);

        $user = User::where('is_banned', false)->where('email', $request->email)->first();
        if (!$user)
            return response(["message" => "User not found"], 404);
        else
            return $user;
    }

    public function searchCountry(Request $request) {
        $this->validate($request, [
            'q' => ['required', 'string']
        ]);

        return DB::table('geo')
            ->where('country', 'like', $request->q.'%')
            ->select('country')
            ->groupBy('country')
            ->limit(15)
            ->get();
    }

    public function searchCity(Request $request) {
        $this->validate($request, [
            'country' => 'string',
            'q' => ['required', 'string']
        ]);

        $where = [
            ['city', 'like', $request->q.'%']
        ];

        if ($request->has('country'))
            $where[] = ['country', '=', $request->country];

            return DB::table('geo')
                ->where($where)
                ->select('city')
                ->limit(15)
                ->get();
    }
}
