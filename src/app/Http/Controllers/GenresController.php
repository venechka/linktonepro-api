<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Genre::orderBy('order', 'asc')->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Genre::class);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $genre = new Genre();
        $genre->name = $request->name;
        $genre->order = $request->order;
        $genre->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre)
    {
        $this->authorize('update', $genre);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $genre->name = $request->name;
        $genre->order = $request->order;
        $genre->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre)
    {
        $this->authorize('delete', $genre);

        $genre->delete();
    }
}
