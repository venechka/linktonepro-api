<?php

namespace App\Http\Controllers;

use App\Models\ConcertType;
use Illuminate\Http\Request;

class ConcertTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConcertType::orderBy('order', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', ConcertType::class);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $concertType = new ConcertType();
        $concertType->name = $request->name;
        $concertType->order = $request->order;
        $concertType->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConcertType  $concertType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConcertType $concertType)
    {
        $this->authorize('update', $concertType);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $concertType->name = $request->name;
        $concertType->order = $request->order;
        $concertType->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConcertType  $concertType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConcertType $concertType)
    {
        $this->authorize('delete', $concertType);

        $concertType->delete();
    }
}
