<?php

namespace App\Http\Controllers;

use App\Models\ArtistType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ArtistTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ArtistType::orderBy('order', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', ArtistType::class);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $artistType = new ArtistType();
        $artistType->name = $request->name;
        $artistType->order = $request->order;
        $artistType->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtistType  $artistType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtistType $artistType)
    {
        $this->authorize('update', $artistType);

        $this->validate($request, [
            "name" => ["required", "string"],
            "order" => ["required", "integer"]
        ]);

        $artistType->name = $request->name;
        $artistType->order = $request->order;
        $artistType->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtistType  $artistType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtistType $artistType)
    {
        $this->authorize('delete', $artistType);

        $artistType->delete();
    }
}
