<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Concert;
use App\Models\Release;
use App\Models\User;
use App\Models\Video;
use App\Services\SlugsService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Test;

class AdminAPIController extends Controller
{
    public function searchUsers(Request $request) {
        $request->validate([
            'query' => ['string', 'max:255']
        ]);

        return User::where('name', 'like', '%' . $request->get('query') . '%')
            ->orWhere('email', 'like', '%' . $request->get('query') . '%')
            ->select('id', 'name', 'email')
            ->limit(10)
            ->get();
    }

    public function userBan (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:users,id'],
            'is_banned' => ['required', 'boolean']
        ]);

        $user = User::find($request->id);
        $user->is_banned = $request->is_banned;
        $user->save();
    }

    public function userAdmin (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:users,id'],
            'is_admin' => ['required', 'boolean']
        ]);

        $user = User::find($request->id);
        $user->is_admin = $request->is_admin;
        $user->save();
    }

    public function searchArtists(Request $request) {
        $request->validate([
            'query' => ['string', 'max:255']
        ]);

        return Artist::where('name', 'like', '%' . $request->get('query') . '%')
            ->select('id', 'name')
            ->limit(10)
            ->get();
    }

    public function banArtist (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:artists,id'],
            'is_banned' => ['required', 'boolean']
        ]);

        $artist = Artist::find($request->id);
        $artist->is_banned = $request->is_banned;
        $artist->save();
    }

    public function banRelease (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:releases,id'],
            'is_banned' => ['required', 'boolean']
        ]);

        $release = Release::find($request->id);
        $release->is_banned = $request->is_banned;
        $release->save();
    }

    public function banVideo (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:videos,id'],
            'is_banned' => ['required', 'boolean']
        ]);

        $video = Video::find($request->id);
        $video->is_banned = $request->is_banned;
        $video->save();
    }

    public function banConcert (Request $request) {
        $this->validate($request, [
            'id' => ['required', 'exists:concerts,id'],
            'is_banned' => ['required', 'boolean']
        ]);

        $concert = Concert::find($request->id);
        $concert->is_banned = $request->is_banned;
        $concert->save();
    }

    public function searchReleases(Request $request) {
        $request->validate([
            'query' => ['string', 'max:255']
        ]);

        return Release::where('name', 'like', '%' . $request->get('query') . '%')
            ->select('id', 'name')
            ->limit(10)
            ->get();
    }

    public function searchVideos(Request $request) {
        $request->validate([
            'query' => ['string', 'max:255']
        ]);

        return Video::where('name', 'like', '%' . $request->get('query') . '%')
            ->select('id', 'name')
            ->limit(10)
            ->get();
    }

    public function sendTestEmail(Request $request) {
        Mail::to('g-worlds@ya.ru')->send(new Test());
    }

    public function createAPIToken(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['exists:users,id'],
        ]);

        $user = User::find($request->user_id);
        $token = $user->createToken('api');

        return ['token' => $token->plainTextToken];
    }

    public function revalidatePath(Request $request)
    {
        $this->validate($request, [
            'path' => 'required|string'
        ]);

        (new SlugsService())->sendRevalidateRequest($request->get('path'));
//        $url = env('PUBLIC_FRONTEND_URL').'/api/revalidate';
//        $client = new Client();
//        $client->request('POST', $url, ['json' => [
//            'secret' => env('PUBLIC_FRONTEND_SECRET'),
//            'path' => $request->get('path')
//        ]]);

    }
}
