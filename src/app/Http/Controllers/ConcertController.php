<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Concert;
use App\Models\Slug as SlugModel;
use App\Rules\Slug;
use App\Rules\SlugExists;
use App\Services\FileService;
use App\Services\SlugsService;
use App\Services\StoriesService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ConcertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if ($request->has('q'))
            $where = [['name', 'like', '%'.$request->q.'%']];

        return Concert::with('artist', 'concert_type')->orderBy('start_datetime', 'desc')
            ->where($where)
            ->paginate(20)->withPath($request->base_url ?? env('ADMIN_FRONTEND_URL'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->has('artist_id'))
            return response(["errors" => ["artist_id" => ["Поле artist_id обязательно для заполнения."]]], 422);

        $artist = Artist::find($request->get('artist_id'));

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $this->authorize('create', [Concert::class, $request->artist_id]);

        $slug = (new Carbon($request->start_datetime))->format("dmy");
        $slugExistCount = SlugModel::where(['artist_slug' => $artist->slug, 'slug' => $slug])->count();
        if ($slugExistCount > 0)
            $slug .= '-'.$slugExistCount;

        $validationData = $request->all();
        $validationData['slug'] = $slug;

        Validator::make($validationData, [
            'slug' => ['string', new SlugExists($artist->slug)],
            'start_datetime' => ['required', 'date'],
            'country' => ['required', 'string'],
            'city' => ['required', 'string'],
            'venue' => ['required', 'string'],
            'concert_type_id' => ['required', 'exists:concert_types,id'],
            'ticket_links' => ['nullable', 'string'],
            'poster' => ['file', 'mimes:jpg,jpeg,png,bmp'],
            'video_link' => ['nullable', 'string'],
            'description' => ['string', 'nullable'],
            'tags' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ])->validate();

        DB::transaction(function () use ($artist, $request, $slug) {
            $concert = new Concert($request->all());

            if ($request->has('poster'))
                $concert->poster_file_id = (new FileService())->storeAndRegister($request->file('poster'), $request->file('poster')->getClientOriginalName(), 'concert_posters', 'Concert');

            $concert->save();

            if ($request->has('stories_files')) {
                (new StoriesService())->store($concert, $request->file('stories_files'));
            }
            (new SlugsService())->create($artist, $concert, $slug);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function show(Concert $concert)
    {
        return $concert->load('artist', 'stories', 'concert_type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Concert $concert)
    {
        $this->authorize('update', $concert);

        $artist = Artist::find($concert->artist_id);

        if (!$artist)
            return response(["errors" => ["artist_id" => ["Артиста с указанным id не существует"]]], 422);

        $validationRules = [
            'start_datetime' => ['date'],
            'country' => ['string'],
            'city' => ['string'],
            'venue' => ['string'],
            'concert_type_id' => ['exists:concert_types,id'],
            'ticket_links' => ['nullable', 'string'],
            'poster' => ['file', 'mimes:jpg,jpeg,png,bmp'],
            'video_link' => ['nullable', 'string'],
            'description' => ['string', 'nullable'],
            'tags' => ['json'],
            'stories' => ['json'],
            'stories_files' => ['array', 'max:3'],
            'stories_files.*' => ['file', 'mimes:mp4','max:15360']
        ];

        $validationData = $request->all();

        $slug = (new Carbon(
            $request->has('start_datetime')
                ? $request->start_datetime
                : $concert->start_datetime))->format('dmy');

        if ($request->has('start_datetime')) {
            $validationData['slug'] = $slug;
        }

        $oldSlug = SlugModel::where(['model' => 'Concert', 'model_id' => $concert->id])->first();

        if (!$oldSlug || $oldSlug['slug'] !== $slug)
            $validationRules['slug'] = ['string', new SlugExists($artist->slug)];

        Validator::make($validationData, $validationRules)->validate();

        DB::transaction(function () use ($request, $oldSlug, $slug, $artist, $concert) {
            $concert->update($request->all());

            if ($request->has('poster')) {
                if ($request->get('poster') === 'null') {
                    (new FileService)->removeAndDelete($concert->poster_file_id);
                    $concert->poster_file_id = null;

                } else {
                    $this->validate($request, [
                        'poster' => ['file', 'mimes:jpg,jpeg,png,bmp', 'max:10240']
                    ]);
                    $concert->poster_file_id = (new FileService)->storeAndRegister($request->file('poster'), $request->file('poster')->getClientOriginalName(), 'concert_posters', "Concert");
                }
            }

            $concert->save();

            if ($request->has('stories')) {
                $stories = json_decode($request->get('stories'));
                (new StoriesService())->update($concert, $stories, $request->file('stories_files'));
            }

            if ($oldSlug)
                (new SlugsService())->update($oldSlug, $slug);
            else
                (new SlugsService())->create($artist, $concert, $slug);

        });
    }

//    public function setBanned(Request $request)
//    {
//        $this->validate($request, [
//            "id" => ['required', 'exists:releases,id'],
//            "banned" => ['required', 'boolean']
//        ]);
//
//        $concert = Concert::find($request->id);
//        $concert->banned = $request->banned;
//        $concert->save();
//    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Concert $concert)
    {
        $this->authorize('delete', $concert);

        DB::transaction(function () use ($concert) {
            $slug = SlugModel::where(['model' => 'Concert', 'model_id' => $concert->id])->first();
            $slug->delete();
            $concert->delete();
        });
    }
}
