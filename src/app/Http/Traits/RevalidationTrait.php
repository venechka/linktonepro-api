<?php
namespace App\Http\Traits;
use App\Services\SlugsService;

trait RevalidationTrait {
    public $afterCommit = true;

    public function saved($model)
    {
        (new SlugsService())->revalidate($model);
    }
}
