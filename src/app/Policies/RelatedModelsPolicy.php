<?php

namespace App\Policies;

use App\Models\Artist;
use App\Models\ArtistManager;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RelatedModelsPolicy
{
    use HandlesAuthorization;

    private function check($user, $artistId)
    {
        if ($user->is_admin)
            return true;

        $artist = Artist::find($artistId);
        if ($user->id === $artist->owner_id)
            return true;

        $isManager = ArtistManager::where('artist_id', $artist->id)->where('user_id', $user->id)->count();
        if ($isManager)
            return true;

        return false;
    }

    public function create(User $user, $artistId)
    {
        return $this->check($user, $artistId);
    }

    public function update(User $user, $model)
    {
        return $this->check($user, $model->artist_id);
    }

    public function delete(User $user, $model)
    {
        return $this->check($user, $model->artist_id);
    }
}
