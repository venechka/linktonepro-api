<?php

namespace App\Policies;

use App\Models\Artist;
use App\Models\ArtistManager;
use App\Models\Concert;
use App\Models\Release;
use App\Models\Story;
use App\Models\User;
use App\Models\Video;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class StoryPolicy
{
    use HandlesAuthorization;

    private function getArtist($story) {
        $artistId = null;

        $release = Release::find($story->release_id);
        if ($release)
            $artistId = $release->artist_id;
        else {
            $video = Video::find($story->video_id);
            if ($video)
                $artistId = $video->artist_id;
            else {
                $concert = Concert::find($story->concert_id);
                if ($concert) $artistId = $concert->artist_id;
            }
        }

        if (!$artistId)
            return false;

        return Artist::find($artistId);
    }

    private function check($user, $story)
    {
        if ($user->is_admin)
            return true;

        $artist = $this->getArtist($story);
        if ($user->id === $artist->owner_id)
            return true;

        $isManager = ArtistManager::where('artist_id', $artist->id)->where('user_id', $user->id)->count();
        if ($isManager)
            return true;

        return false;
    }

    public function create(User $user, Story $story)
    {
//        $story = new \stdClass;
//        $story->release_id = $release_id;
//        $story->video_id = $video_id;
//        $story->concert_id = $concert_id;

        return $this->check($user, $story);
    }

    public function update(User $user, Story $story)
    {
        return $this->check($user, $story);
    }

    public function delete(User $user, Story $story)
    {
        return $this->check($user, $story);
    }
}
