<?php

namespace App\Policies;

use App\Models\Artist;
use App\Models\ArtistManager;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArtistPolicy
{
    use HandlesAuthorization;

    private function check($user, $artist)
    {
        if ($user->is_admin)
            return true;

//        if ($artist->is_banned)
//            return false;

        if ($user->id === $artist->owner_id)
            return true;

        $isManager = ArtistManager::where('artist_id', $artist->id)->where('user_id', $user->id)->count();
        if ($isManager)
            return true;

        return false;
    }

    public function update(User $user, Artist $artist)
    {
        return $this->check($user, $artist);
    }

    public function delete(User $user, Artist $artist)
    {
        return $this->check($user, $artist);
    }
}
