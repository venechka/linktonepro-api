<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SlugExists implements Rule
{
    protected $artistSlug;
    protected $exceptSlug;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($artistSlug, $exceptSlug = null)
    {
        $this->artistSlug = $artistSlug;
        $this->exceptSlug = $exceptSlug;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !\App\Models\Slug::where(['artist_slug' => $this->artistSlug, 'slug' => $value])->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The slug already exists.';
    }
}
