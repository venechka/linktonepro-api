<?php

namespace App\Providers;

use App\Models\Artist;
use App\Models\ArtistBanner;
use App\Models\ArtistType;
use App\Models\Concert;
use App\Models\ConcertType;
use App\Models\Genre;
use App\Models\Release;
use App\Models\ReleaseType;
use App\Models\Story;
use App\Models\TicketOffice;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoType;
use App\Policies\ArtistPolicy;
use App\Policies\RelatedModelsPolicy;
use App\Policies\DictionaryPolicy;
use App\Policies\StoryPolicy;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\Messages\MailMessage;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Artist::class => ArtistPolicy::class,
        Story::class => StoryPolicy::class,
        ArtistBanner::class => RelatedModelsPolicy::class,
        Release::class => RelatedModelsPolicy::class,
        Video::class => RelatedModelsPolicy::class,
        Concert::class => RelatedModelsPolicy::class,
        User::class => DictionaryPolicy::class,
        Genre::class => DictionaryPolicy::class,
        ArtistType::class => DictionaryPolicy::class,
        ReleaseType::class => DictionaryPolicy::class,
        VideoType::class => DictionaryPolicy::class,
        ConcertType::class => DictionaryPolicy::class,
        TicketOffice::class => DictionaryPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(function ($notifiable, $token) {
            return config('app.frontend_url')."/password-reset/$token?email={$notifiable->getEmailForPasswordReset()}";
        });

        ResetPassword::toMailUsing(function ($notifiable, $token) {
            //$url = str_replace(config('app.url'), config('app.frontend_url'), $url);
            $url = config('app.frontend_url')."/password-reset/$token?email={$notifiable->getEmailForPasswordReset()}";
            return (new MailMessage)
                ->subject('Сброс пароля')
                ->line('Мы получили запрос на сброс пароля')
                ->action('Сбросить пароль', $url)
                ->line("Ссылка на сброс пароля будет действительна в течении часа.")
                ->line("Если вы не запрашивали сбор пароля, можете ничего не делать.")
                ->view('emails.reset');
        });

        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            $url = str_replace(config('app.url'), config('app.frontend_url'), $url);
            return (new MailMessage)
                ->subject('Продвердите E-mail адрес')
                ->line('Нажмите на кнопку, чтобы подтвердить E-mail')
                ->action('Подтвердить E-mail', $url)
                ->view('emails.verify');
        });
    }
}
