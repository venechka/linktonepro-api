<?php

namespace App\Providers;

use App\Models\Artist;
use App\Models\Concert;
use App\Models\Release;
use App\Models\Video;
use App\Observers\ArtistObserver;
use App\Observers\ConcertObserver;
use App\Observers\ReleaseObserver;
use App\Observers\VideoObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
//use App\Events\ArtistSaved;
//use App\Events\ReleaseSaved;
//use App\Events\VideoSaved;
//use App\Events\ConcertSaved;
//use App\Listeners\SendShipmentNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
//        ArtistSaved::class => [
//            ArtistObserver::class
//        ],
//        ReleaseSaved::class => [
//            ReleaseObserver::class
//        ],
//        VideoSaved::class => [
//            VideoObserver::class
//        ],
//        ConcertSaved::class => [
//            ConcertObserver::class
//        ],
    ];

    protected $observers = [
        Artist::class => [ArtistObserver::class],
        Release::class => [ReleaseObserver::class],
        Video::class => [VideoObserver::class],
        Concert::class => [ConcertObserver::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
