<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'link',
        'video_type_id',
        'publish_date',
        'artist_id',
        'release_id',
        'description',
        'tags'
    ];

    protected $appends = ['slug'];

    public function getSlugAttribute()
    {
        $slug = Slug::where(['model' => 'video', 'model_id' => $this->id])->first();
        if ($slug)
            return $slug->slug;
    }

    public function video_type()
    {
        return $this->belongsTo(VideoType::class, 'video_type_id', 'id');
    }

    public function release()
    {
        return $this->belongsTo(Release::class);
    }

    public function related_releases() {
        return $this->hasMany(RelatedVideo::class);
    }

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    public function stories()
    {
        return $this->hasMany(Story::class);
    }
}
