<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slug extends Model
{
//    protected $primaryKey = null;

    protected $fillable = [
        'artist_slug',
        'slug',
        'model',
        'model_id'
    ];

    public $timestamps = false;
}
