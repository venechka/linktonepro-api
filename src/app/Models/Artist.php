<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Artist extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'owner_id',
        'name',
        'slug',
        'artist_types_id',
        'genres_id',
        'country',
        'city',
        'description',
        'social_links',
        'members'
    ];

    protected $hidden = [
        'logo_file_id',
        'picture_file_id'
    ];

    protected $appends = ['logo', 'picture'];

    public function getLogoAttribute()
    {
        $file = File::find($this->logo_file_id);
        if ($file)
            return Storage::url($file->path);
    }

    public function getPictureAttribute()
    {
        $file = File::find($this->picture_file_id);
        if ($file)
            return Storage::url($file->path);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function artist_type()
    {
        return $this->belongsTo(ArtistType::class, 'artist_types_id', 'id');
    }

    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genres_id', 'id');
    }

    public function banners()
    {
        return $this->hasMany(ArtistBanner::class);
    }

    public function managers()
    {
        return $this->hasMany(ArtistManager::class);
    }

    public function releases()
    {
        return $this->hasMany(Release::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function concerts()
    {
        return $this->hasMany(Concert::class);
    }
}
