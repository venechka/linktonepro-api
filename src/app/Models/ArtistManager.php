<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtistManager extends Model
{
    use HasFactory;

    protected $fillable = [
        'artist_id',
        'user_id',
    ];

    public $timestamps = ["created_at"]; //only want to used created_at column
    const UPDATED_AT = null; //and updated by default null set

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function artist() {
        return $this->belongsTo(Artist::class);
    }
}
