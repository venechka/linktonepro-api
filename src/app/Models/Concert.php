<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Concert extends Model
{
    use HasFactory;

    protected $fillable = [
        'artist_id',
        'start_datetime',
        'country',
        'city',
        'venue',
        'concert_type_id',
        'poster_file_id',
        'ticket_links',
        'video_link',
        'description',
        'tags'
    ];

    protected $appends = ['poster', 'slug', 'public_ticket_link'];

    protected function getPublicTicketLinkAttribute()
    {
        $value = $this->ticket_links;
        $ticketOffices = TicketOffice::all();
        foreach ($ticketOffices as $ticketOffice) {
            if (strpos($value, $ticketOffice->domain) !== false) {
                if ($ticketOffice->referal)
                    $value = $ticketOffice->referal;
                else
                    $value = $ticketOffice->prefix . $value . $ticketOffice->suffix;
                break;
            }
        }
        return $value;
    }

    public function getSlugAttribute()
    {
        $slug = Slug::where(['model' => 'concert', 'model_id' => $this->id])->first();
        if ($slug)
            return $slug->slug;
    }

    public function concert_type()
    {
        return $this->belongsTo(ConcertType::class, 'concert_type_id', 'id');
    }

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    public function stories()
    {
        return $this->hasMany(Story::class);
    }

    public function getPosterAttribute()
    {
        $file = File::find($this->poster_file_id);
        if ($file)
            return Storage::url($file->path);
    }


}
