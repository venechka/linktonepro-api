<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedVideo extends Model
{
    public function release() {
        return $this->belongsTo(Release::class);
    }

    public function video() {
        return $this->belongsTo(Video::class);
    }
}
