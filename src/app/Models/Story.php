<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Story extends Model
{
    use HasFactory;

    public $timestamps = ["created_at"]; //only want to used created_at column
    const UPDATED_AT = null; //and updated by default null set

    protected $appends = ['file'];

//    public function file() {
//        return $this->belongsTo(File::class); //TODO maybe use relations in other models ?
//    }
//

    public function getFileAttribute () {
        $file = File::find($this->file_id);
        if ($file)
            return Storage::url($file->path).'?response-content-disposition=attachment';
    }

    public function release() {
        return $this->belongsTo(Release::class);
    }

    public function video() {
        return $this->belongsTo(Video::class);
    }

    public function concert() {
        return $this->belongsTo(Concert::class);
    }
}
