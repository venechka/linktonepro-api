<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Release extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'artist_id',
        'release_type_id',
        'description',
        'publish_date',
        'label_company',
        'tracks',
        'social_links',
        'tags'
    ];

    protected $appends = ['cover', 'slug'];

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    public function release_type()
    {
        return $this->belongsTo(ReleaseType::class, 'release_type_id', 'id');
    }

    public function related_videos()
    {
        return $this->hasMany(RelatedVideo::class);
    }

    public function stories()
    {
        return $this->hasMany(Story::class);
    }

//    public function videos()
//    {
//        return $this->hasMany(Video::class);
//    }

    public function getCoverAttribute()
    {
        $file = File::find($this->cover_file_id);
        if ($file)
            return Storage::url($file->path);
    }

    public function getSlugAttribute()
    {
        $slug = Slug::where(['model' => 'release', 'model_id' => $this->id])->first();
        if ($slug)
            return $slug->slug;
    }
}
