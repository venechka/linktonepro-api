FROM php:8.0-fpm-alpine3.14

RUN docker-php-ext-install pdo pdo_mysql
RUN echo "max_file_uploads=100" >> /usr/local/etc/php/conf.d/docker-php-ext-max_file_uploads.ini
RUN echo "post_max_size=100M" >> /usr/local/etc/php/conf.d/docker-php-ext-post_max_size.ini
RUN echo "upload_max_filesize=100M" >> /usr/local/etc/php/conf.d/docker-php-ext-upload_max_filesize.ini